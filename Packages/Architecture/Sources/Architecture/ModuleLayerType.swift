//
//  ModuleLayerType.swift
//  
//
//  Created by Kamil Misiag on 06/12/2020.
//

import Foundation

public protocol ModuleLayerType: AnyObject, Layer {
    associatedtype ViewLayer: ViewLayerType

    func assemble() -> ViewLayer.ViewModelLayer.NavigatorLayer.Controller
}
