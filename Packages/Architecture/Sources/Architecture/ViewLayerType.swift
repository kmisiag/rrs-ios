//
//  ViewLayerType.swift
//  
//
//  Created by Kamil Misiag on 06/12/2020.
//

import Foundation

public protocol ViewLayerType: Layer {
    associatedtype ViewModelLayer: ViewModelLayerType

    var viewModel: ViewModelLayer { get }
}
