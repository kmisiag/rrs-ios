//
//  NavigatorLayerType.swift
//  
//
//  Created by Kamil Misiag on 06/12/2020.
//

import Foundation

public protocol NavigatorLayerType: class, Layer {
    associatedtype Controller
    
    var controller: Controller! { get set }
}
