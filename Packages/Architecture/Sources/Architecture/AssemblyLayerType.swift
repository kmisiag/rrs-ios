//
//  AssemblyLayerType.swift
//  
//
//  Created by Kamil Misiag on 06/12/2020.
//

import Foundation

public protocol AssemblyLayerType: AnyObject, Layer {}
