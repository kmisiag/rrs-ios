//
//  ModuleLayer.swift
//  
//
//  Created by Kamil Misiag on 06/12/2020.
//

import Foundation

open class ModuleLayer<ViewLayer: ViewLayerType>: ModuleLayerType {
    // MARK: - Public Typealiases

    public typealias ViewLayer = ViewLayer

    // MARK: - Initialization

    public init() {}

    // MARK: - Abstract Assemble Method

    open func assemble() -> ViewLayer.ViewModelLayer.NavigatorLayer.Controller { fatalError("Not implemented method \(#function)") }
}
