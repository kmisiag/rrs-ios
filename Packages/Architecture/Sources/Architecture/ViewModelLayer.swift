//
//  ViewModelLayer.swift
//
//
//  Created by Kamil Misiag on 06/12/2020.
//

import Foundation

open class ViewModelLayer<NavigatorLayer: NavigatorLayerType>: ViewModelLayerType {
    // MARK: - Open Properties

    open var navigator: NavigatorLayer

    // MARK: - Initialization

    public init(navigator: NavigatorLayer) {
        self.navigator = navigator
    }
}
