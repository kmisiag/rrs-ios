//
//  ViewModelLayerType.swift
//  
//
//  Created by Kamil Misiag on 06/12/2020.
//

import Foundation

public protocol ViewModelLayerType: AnyObject, Layer {
    associatedtype NavigatorLayer: NavigatorLayerType

    var navigator: NavigatorLayer { get }
}
