//
//  NavigatorLayer.swift
//  
//
//  Created by Kamil Misiag on 06/12/2020.
//

import UIKit

open class NavigatorLayer: NavigatorLayerType {
    // MARK: - Open Properties
    
    open weak var controller: UIViewController!
    
    // MARK: - Initialization
    
    public init() {}
}
