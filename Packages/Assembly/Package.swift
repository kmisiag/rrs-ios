// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Assembly",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "Assembly",
            targets: ["Assembly"]),
    ],
    dependencies: [
        .package(path: "../DI"),
        .package(path: "../Tools"),
        .package(path: "../FeedService"),
        .package(path: "../Architecture"),
        .package(url: "https://github.com/nmdias/FeedKit.git", .upToNextMajor(from: "9.0.0"))
    ],
    targets: [
        .target(
            name: "Assembly",
            dependencies: ["Tools", "FeedService", "FeedKit", "Architecture", "DI"]),
    ]
)
