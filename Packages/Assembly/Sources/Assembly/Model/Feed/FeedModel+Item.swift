//
//  FeedItemModel.swift
//  
//
//  Created by Kamil Misiag on 07/12/2020.
//

import FeedKit
import Foundation

extension FeedModel {
    public struct Item {
        // MARK: - Public Properties

        public var title: String?
        public var description: Description?

        // MARK: - Inits
        
        internal init(model: AtomFeedEntry) {
            title = model.title
            description = Description(summary: model.summary)
        }
        
        internal init(model: RSSFeedItem) {
            title = model.title
            description = Description(attributes: .text, value: model.description)
        }

        internal init(model: JSONFeedItem) {
            title = model.title
            description = Description(attributes: .text, value: model.summary)
        }
    }
}
