//
//  FeedItemModel+Description.swift
//  
//
//  Created by Kamil Misiag on 07/12/2020.
//

import Foundation
import FeedKit

extension FeedModel {
    public class Description {
        public enum Attributes: String {
            case text
            case html
            case xhtml
        }
        
        // MARK: - Public Properties
        
        public let attributes: Attributes?
        public let value: String?
        public var textValue: String? {
            guard attributes == .text else { return nil }
            return value
        }
        public var htmlValue: String? {
            guard attributes == .html else { return nil }
            return value
        }
        public var xhtmlValue: String? {
            guard attributes == .xhtml else { return nil }
            return value
        }

        // MARK: - Init
        
        public init(attributes: Attributes?, value: String?) {
            self.attributes = attributes
            self.value = value
        }
        
        public init(subtitle: AtomFeedSubtitle?) {
            self.attributes = Attributes(rawValue: subtitle?.attributes?.type ?? "")
            self.value = subtitle?.value
        }
        
        public init(summary: AtomFeedEntrySummary?) {
            self.attributes = Attributes(rawValue: summary?.attributes?.type ?? "")
            self.value = summary?.value
        }
    }
}
