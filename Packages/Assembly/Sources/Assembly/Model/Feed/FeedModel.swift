//
//  FeedModel.swift
//  
//
//  Created by Kamil Misiag on 07/12/2020.
//

import Foundation
import FeedKit

public struct FeedModel: ServiceModelDecodable {
    // MARK: - Public Properties
    
    public var title: String?
    public var description: Description?
    public var items: [Item]
    
    // MARK: - Inits
    
    internal init(model: AtomFeed) {
        title = model.title
        description = Description(subtitle: model.subtitle)
        items = model.entries?.map { Item(model: $0) } ?? []
    }
    
    internal init(model: RSSFeed) {
        title = model.title
        description = Description(attributes: .text, value: model.description)
        items = model.items?.map { Item(model: $0) } ?? []
    }

    internal init(model: JSONFeed) {
        title = model.title
        description = Description(attributes: .text, value: model.description)
        items = model.items?.map { Item(model: $0) } ?? []
    }
    
    internal init(from decoder: ServiceDecoder<FeedKit.Feed>) throws {
        switch decoder.serviceModel {
        case .atom(let feed):
            self.init(model: feed)
        case .rss(let feed):
            self.init(model: feed)
        case .json(let feed):
            self.init(model: feed)
        }
    }
}
