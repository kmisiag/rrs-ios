//
//  File.swift
//
//
//  Created by Kamil Misiag on 16/10/2020.
//

import Foundation
import FeedKit
import FeedService

public class FeedListModel: ServiceModelDecodable {
    // MARK: - Public Properties
    
    public let list: [FeedModel]
    
    // MARK: - Init
    
    internal init(feed: FeedModel) {
        self.list = [feed]
    }

    internal init(list: [FeedModel]) {
        self.list = list
    }
    
    internal init(list: [FeedListModel]) {
        self.list = list.flatMap { $0.list }
    }

    internal required init(from decoder: ServiceDecoder<[FeedKit.Feed]>) throws {
        list = decoder.serviceModel.map {
            switch $0 {
            case .atom(let feed):
                return FeedModel(model: feed)
            case .rss(let feed):
                return FeedModel(model: feed)
            case .json(let feed):
                return FeedModel(model: feed)
            }
        }
    }
}
