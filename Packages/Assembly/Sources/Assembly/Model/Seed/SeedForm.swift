//
//  SeedForm.swift
//  
//
//  Created by Kamil Misiag on 10/12/2020.
//

import Combine
import SwiftUI
import Foundation

public class SeedForm: ObservableObject, IAssemblyForm {
    // MARK: - Public Properties
    
    @Binding public var urlString: String
    @Binding public var title: String
    @Binding public var date: Date
    public var url: URL? {
        return URL(string: urlString)
    }

    // MARK: - Init
    
    public required init() {
        _urlString = .constant("")
        _title = .constant("")
        _date = .constant(Date())
    }
    
    // MARK: - Public Methods
    
    public func validate() -> Result<Void, Error>? {
        if url == nil {
            return .failure(ValidationError.invalidURL)
        }
        
        return .success(())
    }
}
