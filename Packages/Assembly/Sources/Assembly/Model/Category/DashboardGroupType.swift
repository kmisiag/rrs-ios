//
//  DashboardGroupType.swift
//  
//
//  Created by Kamil Misiag on 10/12/2020.
//

import Foundation

public enum DashboardGroupType: String, CaseIterable, Codable {
    case categories
    case feeds
}
