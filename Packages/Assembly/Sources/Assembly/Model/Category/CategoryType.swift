//
//  CategoryType.swift
//  
//
//  Created by Kamil Misiag on 04/12/2020.
//

import Foundation

public enum CategoryType: String, CaseIterable {
    case today
    case thisWeek
    case all
    case favourite
}
