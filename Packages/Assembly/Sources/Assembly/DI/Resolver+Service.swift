//
//  Resolver+Service.swift
//  
//
//  Created by Kamil Misiag on 07/12/2020.
//

import Foundation
import DI
import FeedService

extension Resolver {
    internal static let service: Resolver = {
        let resolver = Resolver()
        resolver.registerModule(FeedServiceModule())
        return resolver
    }()
}

