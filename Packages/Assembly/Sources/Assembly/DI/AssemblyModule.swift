//
//  AssemblyModule.swift
//  
//
//  Created by Kamil Misiag on 07/12/2020.
//

import Foundation
import DI

public class AssemblyModule: ModuleType {
    public init() { }
    
    public func load(to resolver: ResolverType) {
        resolver.register(type: IFeedAssembly.self, name: nil, dependencyType: .weak, closure: { FeedAssembly() })
    }
}
