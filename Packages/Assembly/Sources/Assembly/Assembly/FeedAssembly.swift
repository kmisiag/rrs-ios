//
//  FeedAssembly.swift
//  
//
//  Created by Kamil Misiag on 03/12/2020.
//

import DI
import Tools
import Combine
import CombineExt
import Foundation
import FeedService
import Architecture

public protocol IFeedAssembly: AssemblyLayerType {
    func fetchFeed(url: String) -> AnyPublisher<FeedListModel, Error>
    func fetchMultipleFeeds(urls: [String]) -> AnyPublisher<FeedListModel, Error>
}

internal class FeedAssembly: IFeedAssembly {
    internal func fetchFeed(url: String) -> AnyPublisher<FeedListModel, Error> {
        Resolver.resolve(type: IFeedService.self, .service)
            .flatMap { $0.fetchFeed(url: url) }
            .eraseToAnyPublisher()
            .decode(to: FeedModel.self)
            .map { FeedListModel(feed: $0) }
            .dispatchMain()
    }
    
    internal func fetchMultipleFeeds(urls: [String]) -> AnyPublisher<FeedListModel, Error> {
        urls.publisher
            .compactMap { [weak self] url in self?.fetchFeed(url: url) }
            .flatMap { $0 }
            .collect()
            .map { FeedListModel(list: $0) }
            .eraseToAnyPublisher()
    }
}
