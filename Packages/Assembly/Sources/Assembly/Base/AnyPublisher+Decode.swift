//
//  AnyPublisher+Decode.swift
//  
//
//  Created by Kamil Misiag on 02/12/2020.
//

import Combine

extension AnyPublisher {
    internal func decode<T: ServiceModelDecodable>(to type: T.Type, by decoder: ServiceDecoder<Output>? = nil) -> AnyPublisher<T, Error> where T.ServiceModelType == Output {
        tryMap { serviceModel -> T in
            let serviceDecoder = decoder ?? ServiceDecoder(serviceModel)
            return try serviceDecoder.decode(type)
        }
        .eraseToAnyPublisher()
    }
}
