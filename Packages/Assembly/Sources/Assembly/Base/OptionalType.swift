//
//  File.swift
//  
//
//  Created by Kamil Misiag on 02/12/2020.
//

import Foundation

internal protocol OptionalType {
    associatedtype Wrapped
    var asOptional: Wrapped? { get }
}

extension Optional: OptionalType {
    internal var asOptional: Wrapped? {
        self
    }
}
