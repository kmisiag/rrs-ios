//
//  ServiceDecoder.swift
//  
//
//  Created by Kamil Misiag on 02/12/2020.
//

import Foundation

internal class ServiceDecoder<V> {
    // MARK: - Internal Properties
    
    internal let serviceModel: V
    internal var userInfo: [CodingUserInfoKey: Any]
    
    // MARK: - Initialization
    
    internal init(_ serviceModel: V, userInfo: [CodingUserInfoKey: Any] = [:]) {
        self.serviceModel = serviceModel
        self.userInfo = userInfo
    }
    
    // MARK: - Decoding Methods
    
    internal func decode<R>(forKey keyPath: KeyPath<V, R>) throws -> R {
        serviceModel[keyPath: keyPath]
    }
    
    internal func decode<R: ServiceModelDecodable>(_ type: R.Type) throws
        -> R where R.ServiceModelType == V {
        try R(from: self)
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: R.Type, forKey keyPath: KeyPath<V, T>) throws -> R where R.ServiceModelType == T {
        let decoder = ServiceDecoder<T>(serviceModel[keyPath: keyPath], userInfo: userInfo)
        return try R(from: decoder)
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: R.Type, forKey keyPath: KeyPath<V, T?>) throws -> R? where R.ServiceModelType == T {
        guard let value = serviceModel[keyPath: keyPath] else { return nil }
        let decoder = ServiceDecoder<T>(value, userInfo: userInfo)
        return try R(from: decoder)
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V, [T]>) throws -> [R] where R.ServiceModelType == T {
        try serviceModel[keyPath: keyPath].map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V, [T]?>) throws -> [R] where R.ServiceModelType == T {
        try (serviceModel[keyPath: keyPath] ?? []).map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V, [T?]>) throws -> [R] where R.ServiceModelType == T {
        try serviceModel[keyPath: keyPath].compactMap { $0 }.map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V, [T?]?>) throws -> [R] where R.ServiceModelType == T {
        try (serviceModel[keyPath: keyPath] ?? []).compactMap { $0 }.map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
}

// -----------------------------------------------------------------------------
// MARK: - Sequence
// -----------------------------------------------------------------------------

extension ServiceDecoder where V: Sequence {
    internal func decode<R>(forKey keyPath: KeyPath<V.Element, R>) throws -> [R] {
        serviceModel.map {
            return $0[keyPath: keyPath]
        }
    }
    
    internal func decode<R: ServiceModelDecodable>(_ type: R.Type) throws -> [R] where R.ServiceModelType == V.Element {
        try serviceModel.map {
            let decoder = ServiceDecoder<V.Element>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: R.Type, forKey keyPath: KeyPath<V.Element, T>) throws -> [R] where R.ServiceModelType == T {
        try serviceModel.map {
            let decoder = ServiceDecoder<T>($0[keyPath: keyPath], userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: R.Type, forKey keyPath: KeyPath<V.Element, T?>) throws -> [R] where R.ServiceModelType == T {
        try serviceModel.compactMap {
            guard let value = $0[keyPath: keyPath] else { return nil }
            let decoder = ServiceDecoder<T>(value, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
}

// -----------------------------------------------------------------------------
// MARK: - Optionals
// -----------------------------------------------------------------------------

extension ServiceDecoder where V: OptionalType {
    internal func decode<R>(forKey keyPath: KeyPath<V.Wrapped, R>) throws -> R? {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return nil }
        return assemblyModel[keyPath: keyPath]
    }
    
    internal func decode<R: ServiceModelDecodable>(_ type: R.Type) throws -> R? where R.ServiceModelType == V.Wrapped {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return nil }
        let decoder = ServiceDecoder<V.Wrapped>(assemblyModel, userInfo: userInfo)
        return try R(from: decoder)
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: R.Type, forKey keyPath: KeyPath<V.Wrapped, T>) throws -> R? where R.ServiceModelType == T {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return nil }
        let decoder = ServiceDecoder<T>(assemblyModel[keyPath: keyPath], userInfo: userInfo)
        return try R(from: decoder)
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: R.Type, forKey keyPath: KeyPath<V.Wrapped, T?>) throws -> R? where R.ServiceModelType == T {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return nil }
        guard let value = assemblyModel[keyPath: keyPath] else { return nil }
        let decoder = ServiceDecoder<T>(value, userInfo: userInfo)
        return try R(from: decoder)
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V.Wrapped, [T]>) throws -> [R] where R.ServiceModelType == T {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return [] }
        return try assemblyModel[keyPath: keyPath].map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V.Wrapped, [T]?>) throws -> [R] where R.ServiceModelType == T {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return [] }
        return try (assemblyModel[keyPath: keyPath] ?? []).map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V.Wrapped, [T?]>) throws -> [R] where R.ServiceModelType == T {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return [] }
        return try assemblyModel[keyPath: keyPath].compactMap { $0 }.map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
    
    internal func decode<T, R: ServiceModelDecodable>(_ type: [R].Type, forKey keyPath: KeyPath<V.Wrapped, [T?]?>) throws -> [R] where R.ServiceModelType == T {
        guard let assemblyModel = serviceModel as? V.Wrapped else { return [] }
        return try (assemblyModel[keyPath: keyPath] ?? []).compactMap { $0 }.map {
            let decoder = ServiceDecoder<T>($0, userInfo: userInfo)
            return try R(from: decoder)
        }
    }
}
