//
//  IAssemblyForm.swift
//  
//
//  Created by Kamil Misiag on 02/12/2020.
//

import Foundation
import Tools

public protocol IAssemblyForm: class, IModelValidation {
    var identifier: String { get }
    init()
}

// -----------------------------------------------------------------------------
// MARK: - Default Implementation
// -----------------------------------------------------------------------------

extension IAssemblyForm {
    public var identifier: String {
        ""
    }
}

