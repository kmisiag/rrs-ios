//
//  ValidationError.swift
//  
//
//  Created by Kamil Misiag on 13/12/2020.
//

import Foundation

public enum ValidationError: Error {
    case invalidURL
}
