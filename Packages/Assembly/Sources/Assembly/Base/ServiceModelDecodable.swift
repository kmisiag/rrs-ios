//
//  ServiceModelDecodable.swift
//  
//
//  Created by Kamil Misiag on 02/12/2020.
//

import Foundation

internal protocol ServiceModelDecodable: IAssemblyModel {
    associatedtype ServiceModelType
        
    init(from decoder: ServiceDecoder<ServiceModelType>) throws
}
