// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Views",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "Views",
            targets: ["Views"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "Views", dependencies: []),
    ]
)
