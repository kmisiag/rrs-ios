//
//  FeedView.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 26/11/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import SwiftUI

public struct FeedView: View {
    // MARK: - Internal Properties
    
    @Binding internal var title: String
    @Binding internal var subtitle: String?
    @Binding internal var numberOfUnreaded: Int?

    // MARK: - Inits
    
    public init(title: String = "", subtitle: String? = "", numberOfUnreaded: Int? = nil) {
        _title = .constant(title)
        _subtitle = .constant(subtitle)
        _numberOfUnreaded = .constant(numberOfUnreaded)
    }
    
    // MARK: - Body
    
    public var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(title)
                    .font(.headline)
                    .foregroundColor(.primary)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)

                if let subtitle = subtitle {
                    Text(subtitle)
                        .font(.body)
                        .foregroundColor(.secondary)
                        .multilineTextAlignment(.leading)
                        .lineLimit(4)
                }
            }
            
            Spacer()
            
            if let numberOfUnreaded = numberOfUnreaded, numberOfUnreaded > 0 {
                CounterCapsuleView(count: numberOfUnreaded)
            }
            
        }
        .padding(.all, 8.0)
    }
}

struct FeedView_Previews: PreviewProvider {
    internal static var previews: some View {
        FeedView()
    }
}
