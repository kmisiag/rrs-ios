//
//  CategoryView.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 26/11/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import SwiftUI

public struct CategoryView: View {
    // MARK: - Internal Properties
    
    @Binding internal var icon: UIImage?
    @Binding internal var title: String
    @Binding internal var count: Int?

    // MARK: - Inits
    
    public init(icon: UIImage? = nil, title: String = "", numberOfUnreaded: Int? = nil) {
        _icon = .constant(icon)
        _title = .constant(title)
        _count = .constant(numberOfUnreaded)
    }

    // MARK: - Body
    
    public var body: some View {
        HStack {
            if let icon = icon {
                Image(uiImage: icon)
                    .resizable()
                    .aspectRatio(1.0, contentMode: .fit)
            }

            Text(title)
                .font(.subheadline)
                .foregroundColor(.primary)
                .multilineTextAlignment(.leading)
                .lineLimit(2)

            Spacer()
            
            if let count = count, count > 0 {
                CounterCapsuleView(count: count)
            }
            
        }
        .padding(.all, 8.0)
    }
}

struct CategoryView_Previews: PreviewProvider {
    internal static var previews: some View {
        CategoryView()
    }
}
