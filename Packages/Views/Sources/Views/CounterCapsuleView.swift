//
//  CounterCapsuleView.swift
//  
//
//  Created by Kamil Misiag on 10/12/2020.
//

import SwiftUI

public struct CounterCapsuleView: View {
    // MARK: - Internal Properties

    @Binding internal var count: Int

    // MARK: - Inits
    
    public init(count: Int = 0) {
        _count = .constant(count)
    }

    public var body: some View {
        Text("\(count)")
            .font(.body)
            .foregroundColor(.white)
            .padding(EdgeInsets(top: 4, leading: 8, bottom: 4, trailing: 8))
            .background(
                Capsule()
                    .fill(Color.secondary)
            )
    }
}

struct CounterCapsuleView_Previews: PreviewProvider {
    internal static var previews: some View {
        CounterCapsuleView()
    }
}
