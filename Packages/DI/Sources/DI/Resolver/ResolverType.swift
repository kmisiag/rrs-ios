//
//  ResolverType.swift
//  
//
//  Created by Kamil Misiag on on 06/12/2020.
//

import Foundation

public protocol ResolverType {
    func registerModule<T: ModuleType>(_ module: T)
    func register<T>(type: T.Type, name: String?, dependencyType: DependencyType, closure: @escaping (()) -> T)
    func unregister<T>(type: T.Type, name: String?)
    func resolve<T>(type: T.Type, name: String?) throws -> T
}
