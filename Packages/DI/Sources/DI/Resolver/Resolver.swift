//
//  Resolver.swift
//
//
//  Created by Kamil Misiag on on 06/12/2020.
//

import Foundation
import Combine

open class Resolver: ResolverType {
    // MARK: - Private Properties

    private var storage = Storage()

    // MARK: - Initialization

    public required init() {}
    
    // MARK: - Public Methods
    
    public func reset<T>(type: T.Type, name: String? = nil) {
        storage[type, name]?.reset()
    }
    
    public func registerModule<T: ModuleType>(_ module: T) {
        module.load(to: self)
    }
    
    public func register<T>(type: T.Type, name: String?, dependencyType: DependencyType, closure: @escaping (()) -> T) {
        storage[type, name] = Descriptor(type: dependencyType) { closure(()) }
    }
    
    public func unregister<T>(type: T.Type = T.self, name: String? = nil) {
        storage[type, name] = nil
    }
    
    public func resolve<T>(type: T.Type, name: String? = nil) throws -> T {
        guard let descriptor = storage[type, name] else {
            let type = String(describing: T.self)
            let message = #"Not found definition for type \#(type) with name "\#(name ?? "nil")""#
            throw DIError.definitionNotFound(message: message)
        }
        
        return try descriptor.resolve()
    }
    
    public static func resolve<T>(type: T.Type, _ resolver: Resolver, name: String? = nil) -> AnyPublisher<T, Error> {
        Future<T, Error> { result in
            do {
                let resolved = try resolver.resolve(type: type, name: name)
                result(.success(resolved))
            } catch {
                result(.failure(error))
            }
        }
        .eraseToAnyPublisher()
    }
}
