//
//  Descriptor.swift
//
//
//  Created by Kamil Misiag on on 06/12/2020.
//

import Foundation

internal class Descriptor {
    // MARK: - Private Properties

    private let type: DependencyType
    private let producer: () -> Any
    private var instance: Any?

    // MARK: - Initialization

    internal init(type: DependencyType = .weak, producer: @escaping () -> Any) {
        self.type = type
        self.producer = producer
    }
    
    // MARK: - Internal Methods
    
    internal func resolve<T>() throws -> T {
        let instanceTmp = instance ?? producer()
        instance = (type == .strong ? instanceTmp : nil)
        
        guard let result = instanceTmp as? T else {
            throw DIError.invalidType
        }
        
        return result
    }

    internal func reset() {
        instance = nil
    }
}
