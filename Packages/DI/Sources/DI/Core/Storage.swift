//
//  Storage.swift
//
//
//  Created by Kamil Misiag on on 06/12/2020.
//

import Foundation

internal class Storage {
    // MARK: - Private Properties

    private let dispatchQueue = DispatchQueue(label: "com.di.dependency_descriptors_queue")
    private var descriptors = [String: Descriptor]()

    // MARK: - Subscript

    internal subscript<T>(type: T.Type, name: String?) -> Descriptor? {
        get {
            var item: Descriptor?
            let itemId = identifier(for: type, withName: name)
            dispatchQueue.sync { item = descriptors[itemId] }
            return item
        }
        set {
            let item: Descriptor? = newValue
            let itemId = identifier(for: type, withName: name)
            dispatchQueue.sync { descriptors[itemId] = item }
        }
    }
    
    // MARK: - Private Methods
    
    private func identifier<T>(for type: T.Type, withName name: String?) -> String {
        let components = [String(describing: type), name].compactMap { $0 }
        return components.joined(separator: "-")
    }
}
