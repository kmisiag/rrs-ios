//
//  DIError.swift
//  
//
//  Created by Kamil Misiag on on 06/12/2020.
//

import Foundation

public enum DIError: Error {
    case definitionNotFound(message: String)
    case invalidType
}
