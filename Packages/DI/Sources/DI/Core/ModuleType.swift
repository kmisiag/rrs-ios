//
//  ModuleType.swift
//
//
//  Created by Kamil Misiag on on 06/12/2020.
//

import Foundation

public protocol ModuleType: class {
    func load(to resolver: ResolverType)
}
