//
//  DependencyType.swift
//
//
//  Created by Kamil Misiag on on 06/12/2020.
//

import Foundation

public enum DependencyType {
    case strong
    case weak
}
