// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FeedService",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "FeedService",
            targets: ["FeedService"]),
    ],
    dependencies: [
        .package(path: "../DI"),
        .package(url: "https://github.com/nmdias/FeedKit.git", .upToNextMajor(from: "9.0.0"))
    ],
    targets: [
        .target(
            name: "FeedService", dependencies: ["FeedKit", "DI"]),
    ]
)
