//
//  FeedQuery.swift
//  
//
//  Created by Kamil Misiag on 15/10/2020.
//

import Foundation

extension Feed {
    internal class FeedQuery: Query {
        // MARK: Private Properties
        
        private let __url: String

        // MARK: - Init

        public init(url: String) {
            __url = url
        }
        
        // MARK: Internal Methods
        
        internal override func url() -> String {
            __url
        }
    }
}
