//
//  IEngine.swift
//  
//
//  Created by Kamil Misiag on 15/10/2020.
//

import Combine

public protocol IEngine {
    associatedtype QueryType: Query
    associatedtype ResultType
    
    func start(publisher: PassthroughSubject<ResultType, Error>, query: QueryType)
    func stop()
}
