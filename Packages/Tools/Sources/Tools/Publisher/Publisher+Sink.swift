//
//  File.swift
//  
//
//  Created by Kamil Misiag on 13/12/2020.
//

import Combine

extension Publisher where Self.Failure == Never {
    public func sink() ->AnyCancellable {
        sink(receiveValue: { _ in })
    }
}
