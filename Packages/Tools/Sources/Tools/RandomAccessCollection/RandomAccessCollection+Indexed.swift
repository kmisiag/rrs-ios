//
//  RandomAccessCollection+Indexed.swift
//  
//
//  Created by Kamil Misiag on 07/12/2020.
//

import Foundation

extension RandomAccessCollection {
    public func indexed() -> Array<(offset: Int, element: Element)> {
        Array(enumerated())
    }
}
