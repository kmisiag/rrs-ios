//
//  File.swift
//  
//
//  Created by Kamil Misiag on 02/12/2020.
//

import Foundation

public protocol IModelValidation {
    func validate() -> Result<Void, Error>?
}
