//
//  AnyPublisher+DispatchMain.swift
//  
//
//  Created by Kamil Misiag on 07/12/2020.
//

import Foundation
import Combine

extension Publisher {
    public func dispatchMain() -> AnyPublisher<Output, Failure> {
        receive(on: DispatchQueue.main).eraseToAnyPublisher()
    }
}
