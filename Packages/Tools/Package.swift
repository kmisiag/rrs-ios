// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Tools",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "Tools",
            targets: ["Tools"]),
    ],
    dependencies: [
        .package(url: "https://github.com/CombineCommunity/CombineExt", .upToNextMajor(from: "1.2.0"))
    ],
    targets: [
        .target(name: "Tools", dependencies: ["CombineExt"]),
    ]
)
