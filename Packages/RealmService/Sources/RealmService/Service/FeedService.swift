//
//  FeedListModel.swift
//  
//
//  Created by Kamil Misiag on 15/10/2020.
//

import Combine
import FeedKit

public protocol IFeedService: IService {
    func fetchFeed(url: String) -> AnyPublisher<FeedKit.Feed, Error>
}

internal class FeedService: IFeedService {
    internal required init() {}
    
    func fetchFeed(url: String) -> AnyPublisher<FeedKit.Feed, Error> {
        AnyPublisher<FeedKit.Feed, Error>.create(ServiceEngine()) { result in
            let query = Feed.FeedQuery(url: url)
            result(.success(query))
        }
        .eraseToAnyPublisher()
    }
}
