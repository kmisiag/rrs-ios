//
//  IService.swift
//  
//
//  Created by Kamil Misiag on 15/10/2020.
//

import Foundation

public protocol IService {
    init()
}
