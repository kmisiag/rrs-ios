//
//  AnyPublisher+Create.swift
//  
//
//  Created by Kamil Misiag on 15/10/2020.
//

import Combine

extension AnyPublisher {
    public typealias SubscribeClosure<T> = (AnyPublisher<T, Error>) -> AnyCancellable

    public static func create<T: IEngine>(_ engine: T, _ subscribe: @escaping (@escaping Future<T.QueryType, Error>.Promise) -> Void) -> AnyPublisher<Output, Error> where T.ResultType == Output {
        Future<T.QueryType, Error>(subscribe)
            .flatMap { AnyPublisher.create(engine, query: $0) }
            .eraseToAnyPublisher()
    }

    private static func create<T: IEngine>(_ engine: T, query: T.QueryType) -> AnyPublisher<T.ResultType, Error> {
        let subject = PassthroughSubject<T.ResultType, Error>()

        return subject
            .handleEvents(receiveSubscription: { _ in engine.start(publisher: subject, query: query) }, receiveCancel: { engine.stop() })
            .eraseToAnyPublisher()
    }
}
