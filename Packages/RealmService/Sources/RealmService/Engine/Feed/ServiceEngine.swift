//
//  ServiceEngine.swift
//  
//
//  Created by Kamil Misiag on 15/10/2020.
//

import Combine
import Foundation
import FeedKit

internal enum ServiceEngineError: Error {
    case invalidUrl
}

internal class ServiceEngine: IEngine {
    internal typealias QueryType = HttpQuery
    internal typealias ResultType = FeedKit.Feed

    // MARK: - Private Properties
    
    private var parser: FeedParser?
    
    // MARK: - Internal Methods
    
    internal func start(publisher: PassthroughSubject<ResultType, Error>, query: QueryType) {
        guard let feedURL = URL(string: query.url()) else { publisher.send(completion: .failure(ServiceEngineError.invalidUrl)); return }
        
        parser = FeedParser(URL: feedURL)
        
        parser?.parseAsync(result: { result in
            switch result {
            case .success(let feed):
                publisher.send(feed)
                publisher.send(completion: .finished)
            case .failure(let error):
                publisher.send(completion: .failure(error))
            }
        })
    }
    
    internal func stop() {
        parser?.abortParsing()
    }
}
