//
//  Query.swift
//  
//
//  Created by Kamil Misiag on 15/10/2020.
//

import Foundation

open class Query: IQuery {    
    open func url() -> String {
        fatalError("func url() -> String not implemented")
    }
}
