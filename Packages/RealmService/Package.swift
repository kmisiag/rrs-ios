// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "RealmService",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "RealmService",
            targets: ["RealmService"]),
    ],
    dependencies: [
        .package(path: "../DI"),
        .package(name: "Realm", url: "https://github.com/realm/realm-cocoa.git", .upToNextMajor(from: "10.2.0")),
    ],
    targets: [
        .target(
            name: "RealmService", dependencies: ["DI", "Realm"]),
    ]
)
