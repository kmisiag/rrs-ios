//
//  CategoryType+Icon.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 10/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import UIKit
import Assembly

extension CategoryType {
    internal var icon: UIImage? {
        switch self {
        case .today:
            return UIImage(systemName: "sun.max")
        case .thisWeek:
            return UIImage(systemName: "calendar")
        case .all:
            return UIImage(systemName: "bell")
        case .favourite:
            return UIImage(systemName: "heart")
        }
    }
}
