//
//  Resolver+Assembly.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 07/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import Foundation
import DI
import Assembly

extension Resolver {
    internal static let assembly: Resolver = {
        let resolver = Resolver()
        resolver.registerModule(AssemblyModule())
        return resolver
    }()
}
