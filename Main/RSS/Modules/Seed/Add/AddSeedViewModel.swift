//
//  AddSeedViewModel.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 13/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import DI
import Assembly
import Foundation
import Architecture

internal class AddSeedViewModel: ViewModelLayer<AddSeedNavigation>, ObservableObject {
    // MARK: - Internal Properties
    
    internal let form = SeedForm()
    
    // MARK: - Actions
    
    public func addAction() {
        
    }
    
    public func appearAction() {
    }
}
