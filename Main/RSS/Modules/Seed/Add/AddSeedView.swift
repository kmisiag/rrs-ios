//
//  AddSeedView.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 13/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import Views
import Tools
import SwiftUI
import Assembly
import Foundation
import Architecture

internal struct AddSeedView: View, ViewLayerType {
    // MARK: - Internal Properties

    @ObservedObject internal var viewModel: AddSeedViewModel
    
    // MARK: - Body Definition

    internal var body: some View {
        Form {
            TextField("URL", text: viewModel.form.$urlString)
            TextField("Title", text: viewModel.form.$title)
        }
        .navigationBarItems(trailing: Button(action: { viewModel.addAction() }, label: { Image(uiImage: UIImage(systemName: "plus")!).foregroundColor(.accentColor) }))

        .onAppear { viewModel.appearAction() }
    }
}

