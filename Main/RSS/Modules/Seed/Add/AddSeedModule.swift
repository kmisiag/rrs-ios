//
//  AddSeedModule.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 13/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import UIKit
import SwiftUI
import Architecture

internal class AddSeedModule: Architecture.ModuleLayer<AddSeedView> {
    internal override func assemble() -> UIViewController {
        let navigator = ViewLayer.ViewModelLayer.NavigatorLayer()
        let viewModel = ViewLayer.ViewModelLayer(navigator: navigator)
        let view = ViewLayer(viewModel: viewModel)
        let vc = UIHostingController(rootView: view)
        
        view.viewModel.navigator.controller = vc

        return vc
    }
}
