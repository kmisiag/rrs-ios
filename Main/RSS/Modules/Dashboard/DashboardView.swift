//
//  DashboardView.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 06/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import Views
import Tools
import SwiftUI
import Assembly
import Foundation
import Architecture

internal struct DashboardView: View, ViewLayerType {
    // MARK: - Internal Properties

    @ObservedObject internal var viewModel: DashboardViewModel
    
    // MARK: - Body Definition

    internal var body: some View {
        Form {
            ForEach(Assembly.DashboardGroupType.allCases.indexed(), id: \.offset) { (offset, element) in
                if element == .categories {
                    List(CategoryType.allCases.indexed(), id: \.offset) { (offset, element) in
                        CategoryView(icon: element.icon, title: element.rawValue, numberOfUnreaded: 0)
                    }
                    .fixedSize()
                } else if element == .feeds {
                    Section {
                        DisclosureGroup(element.rawValue) {
                            List(viewModel.feedList?.list.indexed() ?? [], id: \.offset) { (offset, element) in
                                FeedView(title: element.title ?? "", subtitle: element.description?.textValue ?? "", numberOfUnreaded: 0)
                            }
                        }
                    }
                }
            }
        }
        .navigationBarItems(trailing: Button(action: { viewModel.addAction() }, label: { Image(uiImage: UIImage(systemName: "plus")!).foregroundColor(.accentColor) }))
        .onAppear { viewModel.appearAction() }
    }
}

