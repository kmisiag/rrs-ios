//
//  DashboardNavigator.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 06/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import UIKit
import Combine
import Foundation
import Architecture

internal class DashboardNavigator: NavigatorLayer {
    // MARK: - Private Properties

    private var cancellables = Set<AnyCancellable>()

    // MARK: - Internal Properties

    internal func presentAddSeedModule() -> AnyPublisher<Void, Never> {
        let viewController = AddSeedModule().assemble()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let subject = PassthroughSubject<Void, Never>()
        
        return subject
            .handleEvents(receiveSubscription: { [weak self] _ in self?.controller.present(navigationController, animated: true, completion: { subject.send(completion: .finished) })})
            .eraseToAnyPublisher()
    }
}
