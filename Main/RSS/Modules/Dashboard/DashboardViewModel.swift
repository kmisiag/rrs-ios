//
//  DashboardViewModel.swift
//  RSS-iOS
//
//  Created by Kamil Misiag on 06/12/2020.
//  Copyright © 2020 Kamil Misiag. All rights reserved.
//

import DI
import Tools
import Combine
import SwiftUI
import Assembly
import Foundation
import CombineExt
import Architecture

internal class DashboardViewModel: ViewModelLayer<DashboardNavigator>, ObservableObject {
    // MARK: - Private Properties
    
    private var cancellables = Set<AnyCancellable>()
    @Inject(.assembly) private var feedAssembly: IFeedAssembly
    
    // MARK: - Internal Properties
    
    @Published internal var feedList: FeedListModel?
    
    // MARK: - Actions
    
    public func appearAction() {
        fetchFeeds()
    }
    
    public func addAction() {
        navigator.presentAddSeedModule()
            .sink()
            .store(in: &cancellables)
    }
    
    // MARK: - Private Methods
    
    private func fetchFeeds() {
        let urls = ["https://swiftbysundell.com/rss", "https://swiftbysundell.com/podcast/rss"]
        let publisher = feedAssembly.fetchMultipleFeeds(urls: urls)
            .materialize()
            .share(replay: 1)
            
        publisher
            .values()
            .map { Optional($0) }
            .assign(to: &$feedList)
                
        publisher
            .failures()
            .sink(receiveValue: { print($0) })
            .store(in: &cancellables)
    }
}
